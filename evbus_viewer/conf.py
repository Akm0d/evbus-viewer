CONFIG = {
    "config": {
        "default": None,
        "help": "Load extra options from a configuration file onto hub.OPT.evbus_viewer",
    }
}

SUBCOMMANDS = {}

CLI_CONFIG = {
    "config": {"options": ["-c"]},
    "acct_file": {
        "source": "acct",
        "os": "ACCT_FILE",
    },
    "acct_key": {
        "source": "acct",
        "os": "ACCT_KEY",
    },
}

DYNE = {"evbus_viewer": ["evbus_viewer"], "consumer": ["consumer"], "tool": ["tool"]}
