import datetime
import json

try:
    import aiokafka.consumer

    HAS_LIBS = (True,)
except ImportError as e:
    HAS_LIBS = False, str(e)

__virtualname__ = "kafka"


def __virtual__(hub):
    return HAS_LIBS


async def listen(hub, ctx):
    topics = ctx.acct.get("topics", [])
    async with aiokafka.consumer.AIOKafkaConsumer(
        *topics, bootstrap_servers=ctx.acct.connection.bootstrap_servers
    ) as consumer:
        async for message in consumer:
            data = json.loads(message.value.decode())
            data["provider"] = ctx.provider_name
            data["profile"] = ctx.profile_name
            data["timestamp"] = str(datetime.datetime.now())
            yield data
