import datetime
import json

try:
    import aio_pika

    HAS_LIBS = (True,)
except ImportError as e:
    HAS_LIBS = False, str(e)

__virtualname__ = "pika"


def __virtual__(hub):
    return HAS_LIBS


async def listen(hub, ctx):
    routing_key = ctx.acct.get("routing_key", "")
    async with await aio_pika.connect(**ctx.acct.connection) as conn:
        async with conn.channel() as channel:
            queue = await channel.declare_queue(routing_key)
            async with queue.iterator() as q:
                async for message in q:
                    data = json.loads(message.body.decode())
                    data["provider"] = ctx.provider_name
                    data["profile"] = ctx.profile_name
                    data["timestamp"] = str(datetime.datetime.now())
                    yield data
