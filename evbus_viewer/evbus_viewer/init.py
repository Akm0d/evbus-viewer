from typing import Any
from typing import Dict
from typing import List

import dict_tools.data

__func_alias__ = {"filter_": "filter"}


def __init__(hub):
    hub.pop.sub.add(dyne_name="loop")
    hub.pop.sub.add(dyne_name="evbus")
    hub.pop.sub.add(dyne_name="consumer")
    hub.pop.sub.add(dyne_name="tool")
    hub.pop.sub.load_subdirs(hub.tool, recurse=True)
    hub.pop.sub.load_subdirs(hub.evbus_viewer, recurse=True)


TABLE_HEADER = ["timestamp", "provider", "profile", "run_name", "tags", "message"]


def cli(hub):
    hub.pop.config.load(["evbus_viewer", "acct", "evbus"], cli="evbus_viewer")
    hub.pop.sub.add(dyne_name="idem")

    hub.loop.qt.APP = hub.tool.qt.widget.QApplication([])
    main_layout = hub.evbus_viewer.init.layout()  # noqa
    hub.pop.loop.create("qt")

    hub.pop.Loop.run_until_complete(hub.evbus_viewer.init.main())


def layout(hub):
    # All widgets will be added under this layout
    main_layout = hub.tool.qt.widget.QVBoxLayout()

    # Create all widgets for the app
    # A List of events that can be sorted by column
    list_items = hub.tool.qt.widget.QTableWidget(0, len(TABLE_HEADER))
    column_header = list_items.horizontalHeader()
    column_header.setStretchLastSection(True)
    list_items.setHorizontalHeaderLabels(TABLE_HEADER)
    list_items.setSortingEnabled(True)
    hub.evbus_viewer.ITEMS = list_items

    # Add all widgets to the layout
    main_layout.addWidget(list_items)

    # Build the main app window
    main_window = hub.tool.qt.widget.QWidget()
    main_window.setWindowTitle("POP Evbus Viewer")
    main_window.setLayout(main_layout)
    main_window.show()
    main_window.activateWindow()

    return main_window


async def main(hub):
    # Collect evbus profiles
    profiles = await hub.evbus.acct.profiles(
        acct_file=hub.OPT.acct.acct_file, acct_key=hub.OPT.acct.acct_key
    )

    if not profiles:
        return

    # Run the listener
    task = hub.pop.Loop.create_task(await hub.evbus_viewer.init.listener(profiles))
    await hub.pop.loop.sleep(0)

    try:
        return hub.loop.qt.APP.exec()
    finally:
        task.cancel()
        await task


async def listener(hub, profiles: Dict[str, List[Dict[str, Any]]]):
    for provider in profiles:
        if provider in hub.consumer:
            hub.log.debug(f"Found provider: {provider}")
        else:
            hub.log.debug(f"No provider: {provider}")

    generators = []
    for provider_name, provider in profiles.items():
        for profile in provider:
            for profile_name, profile_details in profile.items():
                ctx = dict_tools.data.NamespaceDict(
                    provider_name=provider_name,
                    profile_name=profile_name,
                    acct=dict_tools.data.SafeNamespaceDict(profile_details),
                )
                hub.log.info(f"Connecting to {provider_name} listener: {ctx.acct}")
                generators.append(hub.consumer[provider_name].listen(ctx))

    hub.log.info("initialized generators")
    async for event in hub.pop.loop.as_yielded(generators):
        hub.evbus_viewer.init.add_row(event)


def add_row(hub, event):
    TOP = 0
    # Add a row to the top of the table
    hub.evbus_viewer.ITEMS.insertRow(TOP)

    # Edit each cell of the table to have values from the current event
    for column, name in enumerate(TABLE_HEADER):
        text = str(event.get(name))
        item = hub.tool.qt.widget.QTableWidgetItem(text)
        hub.evbus_viewer.ITEMS.setItem(TOP, column, item)
