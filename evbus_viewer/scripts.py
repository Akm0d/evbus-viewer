#!/usr/bin/env python3
import pop.hub


def start():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="evbus_viewer", omit_class=False)
    hub.evbus_viewer.init.cli()
