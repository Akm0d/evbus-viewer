try:
    import PyQt6.QtWidgets

    HAS_LIBS = (True,)
except ImportError as e:
    HAS_LIBS = False, str(e)


def __virtual__(hub):
    return HAS_LIBS


def __init__(hub):
    hub.pop.sub.dynamic(
        sub=hub.tool.qt,
        subname="widget",
        resolver=hub.tool.qt.init.resolver,
        context=None,
    )


def resolver(hub, path: str, context):
    widget = path.split(".")[1]
    return getattr(PyQt6.QtWidgets, widget)
