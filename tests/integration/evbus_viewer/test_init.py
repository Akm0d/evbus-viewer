from unittest import mock


def test_cli(hub):
    with mock.patch("sys.argv", ["evbus-viewer"]):
        hub.evbus_viewer.init.cli()
