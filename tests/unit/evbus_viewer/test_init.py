def test_cli(mock_hub, hub):
    mock_hub.evbus_viewer.init.cli = hub.evbus_viewer.init.cli
    mock_hub.evbus_viewer.init.cli()
    mock_hub.pop.config.load.assert_called_once_with(["evbus_viewer"], "evbus_viewer")
